---
title: Spring MVC初探
date: 2016-03-17 19:00:28
tags: Spring, Spring MVC
---
Spring MVC 是 Spring 框架中用于 Web 应用快速开发的一个模块。MVC 即 Model-View-Controller 的缩写，是广泛应用在 Web 应用开发中的设计模式。Spring MVC 基于 Spring 框架、Servlet 和 JSP（JavaServer Page）。

## 优点
Spring MVC 在 Spring 框架的基础之上，可以说给企业软件开发带来了快速开发、迭代开发、模块化等优点。首先，Spring MVC 提供了 Controller、JavaBean Model、View 之间不同角色之间清晰的分工，使得开发的软件耦合度低，方便软件扩展。其次，Spring MVC 非常灵活，它完全基于 interface 编程易于配置及扩展，同时提供了一些实用的实现。而且，Spring MVC 是与视图无关的，不一定需要使用 JSP，可以实用 FreeMaker、Velocity 等其他的视图。它的 Controller 可以像其他对象一样使用 IoC 进行配置，方便测试、并与其他对象集成。

## DispatcherServlet
在 Spring MVC 中，所有收到了请求都经过需要经过容器（如Tomcat）后，交给一个 Servlet 进行处理并分发。Spring Web MVC 提供了开箱即用的 Dispatcher Servlet，其全称是 org.springframework.web.servlet.DispatcherServlet。其主要的职责就是根据收到的请求进行分发，并调度各部分处理请求。要实用这个 servlet，需要在 web.xml 文件中应用 servlet 和 servlet-mapping 元素，如下：
```xml
<servlet>
  <servlet-name>springmvc</servlet-name>
  <servlet-class>
    org.springframework.web.servlet.DispatherServlet
  </servlet-class>
  <load-on-startup>1</load-on-startup>
</servlet>

<servlet-mapping>
  <servlet-name>springmvc</servlet-name>
  <url-pattern>/</url-pattern>
</servlet-mapping>
```
load-on-startup: 表示在容器启动时初始化该 Servlet；
url-pattern: 表示哪些请求交给 springmvc 处理。

## Controller
Controller 即控制器，负责处理某个请求。其职责一般包括：对收到的请求进行验证并绑定到特定的命令对象上，并将其交给业务对象，又业务对象处理并返回模型数据及视图 ModelAndView。该接口包含了一个函数
```java
ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response);
```
其实现类可以访问请求的 HttpServletRequest 和 HttpServletResponse，对请求进行处理并返回视图路径或视图路径和模型的 ModelAndView 对象。Controller 接口的实现类只能处理一个单一动作（Action），而一个基于注解（@Controller）的控制器可以同时支持多个请求处理动作且无须实现任何接口。

## 结语
Spring MVC 可以说给 Java 带来了一股新鲜的空气，简单易用，配合 Java 简单的语法以及设计模式的经验，并且充分利用基于接口编程的可扩展性，极大地提高了 Web 应用的开发。以后在进行深入的过程中，可以多了解了解 Spring 框架的实现，并在平时编程中多加应用，相信对个人能力是一个极大的提高。